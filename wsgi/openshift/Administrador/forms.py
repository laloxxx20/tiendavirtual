# -*- coding: utf-8 -*-
from django import forms
from Nucleo.Promocion import *
from Nucleo.Usuario import *
from Nucleo.ProductoCreador import *
from Nucleo.SonidoCreador import *
from Nucleo.VideoCreador import *
from Nucleo.ImagenCreador import *
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from Productos.models import TIPOS,EXTENSIONES,MIME_TYPES,CATEGORIAS
import datetime
import time
from Productos.models import ProductosBD

class inicio_sesion_admi(forms.Form):
	usuario = forms.CharField(widget = forms.TextInput( attrs = {'class' : 'login username-field',
																'name':'username',
																'id':'username',
																'placeholder':'Usuario',
																'required':'', }))
	clave = forms.CharField(widget = forms.PasswordInput(attrs = {
																  'id':'password',
																  'name':'password',
																  'placeholder':'Contraseña',
																  'class':'login password-field' ,
																  'required':'',}))

	def clean(self):
		prueba=Usuario()
		saldo=prueba.saldo_actual("danet",10.89)
		print "saldo: ",saldo 
		cleaned_data = super(inicio_sesion_admi, self).clean()
		usuario = cleaned_data.get('usuario')
		print "usuario: ",usuario
		clave = cleaned_data.get('clave')
		print "clave: ",clave

		user = authenticate(username=usuario, password=clave)
		if user is not None:
			if not user.is_active:
				raise forms.ValidationError("Usuario no activo")			
		else:
			raise forms.ValidationError("Usuario o Contraseña equivocada! ")

		return cleaned_data


class UploadForm(forms.ModelForm):
    class Meta:
        model = ProductosBD
        fields = ['archivo']

class adherir_producto_form(forms.Form):
	archivo = UploadForm	

	tipo = forms.ChoiceField(choices=TIPOS, widget= forms.Select(attrs = {'class' : 'selectpicker',
																			'name':'tipos',
																			'id':'opciones_tipos',
																			'required':''}																									
																			))
	mime_type = forms.ChoiceField(choices=MIME_TYPES, widget= forms.Select(attrs = {'class' : 'selectpicker',
																			'name':'myme_types',
																			'id':'opciones_mime_types',															
																			'required':''}
																			))
	extension = forms.ChoiceField(choices=EXTENSIONES, widget= forms.Select(attrs = {'class' : 'selectpicker',
																			'name':'extensiones',
																			'id':'opciones_extension',															
																			'required':''}
																			))	

	nombre_archivo = forms.CharField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																'name':'nombre_archivo',
																'id':'nombre_archivo',
																'placeholder':'Nombre archivo',
																'required':'', }))
	nombre_autor = forms.CharField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																'name':'nombre_autor',
																'id':'nombre_autor',
																'placeholder':'Nombre Autor',
																'required':'', }))
	precio = forms.FloatField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																'name':'precio',
																'id':'precio',
																'placeholder':'$ 0.00',
																'required':'', }))
	categoria = forms.ChoiceField(choices=CATEGORIAS, widget= forms.Select(attrs = {'class' : 'selectpicker',
																			'name':'categorias',
																			'id':'opciones_categorias',															
																			'required':''}
																			))


	def clean(self):
		cleaned_data = super(adherir_producto_form, self).clean()
		return cleaned_data

	def subir(self):
		manejador=ProductoCreador()
		globalManejador= manejador.Fun_Categorias()

		videoMane=VideoCreador()
		globalManejador= videoMane.Fun_Categorias()

		data=[]
		data.append(self.cleaned_data['tipo'])
		data.append(self.cleaned_data['nombre_autor'])
		data.append(self.cleaned_data['extension'])
		data.append(self.cleaned_data['mime_type'])
		data.append(self.cleaned_data['categoria'])
		data.append(self.cleaned_data['precio'])
		data.append(self.cleaned_data['nombre_archivo'])

		globalManejador.Registrar_producto(data)
		




class cambiar_clave(forms.Form):
	clave = forms.CharField(widget = forms.PasswordInput(attrs = {
																  'id':'password',
																  'name':'password',
																  'placeholder':'Contraseña',
																  'class':'navbar-tex' ,
																  'required':'',}))
	def clean(self):
		pass


class adherir_promocion(forms.Form):
	
	fecha_inicio= forms.DateField(initial = datetime.date.today(), widget=forms.TextInput(attrs = {'class': 'datepicker'}))

	fecha_fin= forms.DateField(initial = datetime.date.today(), widget=forms.TextInput(attrs = {'class': 'datepicker'}))

	descuento = forms.FloatField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																	'name':'descuento',
																	'id':'descuento',
																	'placeholder':'$ 0.00',
																	'required':'', }))
	descripcion = forms.CharField(widget = forms.Textarea)

	def clean(self):
		cleaned_data = super(adherir_promocion, self).clean()		
		fecha_inicio=cleaned_data.get("fecha_inicio")
		fecha_fin=cleaned_data.get("fecha_fin")
		descuento=cleaned_data.get("descuento")
		descripcion=cleaned_data.get("descripcion")

		return cleaned_data

	def registrarDatos(self):
		fecha_inicio_db=self.cleaned_data['fecha_inicio']
		print fecha_inicio_db
		fecha_fin_db=self.cleaned_data['fecha_fin']
		descuento_db=self.cleaned_data['descuento']
		descripcion_db=self.cleaned_data['descripcion']

		promocionFinal=Promocion()
		# promocionFinal.Registrar(233424,123123,4,"descripcion_db")

		print fecha_inicio_db.year
		fecha_ini_integer = time.mktime(datetime.date(fecha_inicio_db.year,fecha_inicio_db.month,fecha_inicio_db.day).timetuple())		
		fecha_fin_integer = time.mktime(datetime.date(fecha_fin_db.year,fecha_fin_db.month,fecha_fin_db.day).timetuple())		
		promocionFinal.Registrar(fecha_ini_integer,fecha_fin_integer,descuento_db,descripcion_db)	



class cargar_dinero(forms.Form):
       nombre_usuario = forms.CharField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																'name':'nombre_usuario',
																'id':'nombre_usuario',
																'placeholder':'Nombre Usuario',
																'required':'', }))
       saldo = forms.FloatField(widget = forms.TextInput( attrs = {'class' : 'navbar-tex',
																'name':'precio',
																'id':'saldo',
																'placeholder':'$ 0.00',
																'required':'', }))
