from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response# from Nucleo.Usuario import *
from Administrador.forms import *
from django.core.urlresolvers import reverse# Create your views here.
from django.contrib.auth import authenticate, login, logout
from Nucleo.Usuario import *
from filetransfers.api import prepare_upload, serve_file


def perfil_admi(request):

	if request.method == 'POST':
		if request.POST['form-type']=="admin_product_form":
			form_productos = adherir_producto_form(request.POST,request.FILES)
			if form_productos.is_valid():
				form_productos.subir()
				return HttpResponseRedirect( reverse ('Administrador:perfil_admi') )

		if request.POST['form-type']=="password_form":
			form_cambiar_claves= cambiar_clave(request.POST)

		if request.POST['form-type']== "promocion_form":
			print "ENTRO A PROMCION FROM"
		 	form_adherir_promocion= adherir_promocion(request.POST)
			if form_adherir_promocion.is_valid():
		 		form_adherir_promocion.registrarDatos()
		 		return HttpResponseRedirect( reverse ('Administrador:perfil_admi') )

		if request.POST['form-type']=="cargar_dinero_form":
			print "entra form de danet"
			form_cargardin= cargar_dinero(request.POST)
			
	else:		
		form_productos = adherir_producto_form()
		form_cambiar_claves = cambiar_clave()
		form_adherir_promocion = adherir_promocion();
		form_cargardin = cargar_dinero()

	context={'title':'Perfil Admi','form_adherir_productos':form_productos,
			'form_cambiar_clave':form_cambiar_claves ,'form_adherir_promocion':form_adherir_promocion,
			'form_cargar_dinero':form_cargardin}

	return render(request,'perfil.html',context)
	

def inicio_sesi_admi(request):	
	#prueba=Usuario()
	#prueba.eliminar("eh")

	if request.method == 'POST':
		form =inicio_sesion_admi(request.POST)
		if form.is_valid():
			usuario = form.cleaned_data['usuario']
			clave = form.cleaned_data['clave']
			usuarioAdmin = authenticate(username=usuario, password=clave)
			login(request, usuarioAdmin)
			return HttpResponseRedirect( reverse ('Administrador:perfil_admi') )
	else:		
		form = inicio_sesion_admi()

	context={'title':'Inicio Sesion Admin','formIniSesiAdmi':form }		
	return render(request,'inicio_sesi_admi.html',context)		

def cerrar_sesion_admi(request):
	logout(request)	
	return HttpResponseRedirect( reverse ('Administrador:inicio_sesi_admi') )	


# testing with the uploader
def upload_handler(request):
	view_url = reverse('Productos:index')
	if request.method == 'POST':
		print "form with info"
		form = UploadForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			print "entro a guardar"
		else:	
			print "no entro a guardar"
			
		return HttpResponseRedirect(view_url)

	else:
		print "form without info"
		upload_url, upload_data = prepare_upload(request, view_url)
		form = UploadForm()

	context={'form': form, 'upload_url': upload_url, 'upload_data': upload_data,
			'uploads': UploadModel.objects.all(), 'error': request.GET.get('error')}    
	return render(request, 'upload.html',context)