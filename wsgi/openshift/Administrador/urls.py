from django.conf.urls import patterns, url
from Administrador import views
from django.contrib.auth.decorators import login_required

urlpatterns = patterns('',
	url(r'^perfil/$', login_required(views.perfil_admi), name='perfil_admi'),
	url(r'^inicio_sesion/$', views.inicio_sesi_admi, name='inicio_sesi_admi'),
	url(r'^cerrar_sesion/$', views.cerrar_sesion_admi, name='cerrar_sesion_admi'),
	url(r'^uploader/$', views.upload_handler, name='upload_handler'),
	
	# url(r'^probandoproyecto/$', views.probando, name='probando'),
	# url(r'^ingresar_producto/$', views.creando_user, name='ingresar_producto'),
	# url(r'^iniciarsesion/$',views.iniciar_sesion, name='iniciar_sesion'),
)