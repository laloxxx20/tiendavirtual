from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response# from Nucleo.Usuario import *
from Compras.forms import *


def creando_compras(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			form.registrar()
			return HttpResponseRedirect ( reverse ('Descargas:iniciar_sesion') )
	else:
		form = RegisterForm()		
	
	context={'title':'Registrarse','formRegister':form }	
	return render(request,'registrarseCompras.html',context)