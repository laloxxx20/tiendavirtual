# from Productos.models import PersonaBD
from BaseDatos.genericaBD import *

# Create your models here.
class UsuarioGene(GenericaDB):	

	@abc.abstractproperty	
	def obtenerUno(self,data):
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificardor):
	 	pass	
		

class ProductoGene(GenericaDB):

	@abc.abstractproperty	
	def obtenerUno(self,data):
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificardor):
	 	pass

class PromocionGene(GenericaDB):
	@abc.abstractproperty	
	def obtenerUno(self,data):
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificardor):
	 	pass

class DescargaGene(GenericaDB):
	@abc.abstractproperty	
	def obtenerUno(self,data):
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificardor):
		pass

class AsministradorGene(GenericaDB):
	@abc.abstractproperty	
	def obtenerUno(self,data):
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificardor):
		pass

 	@abc.abstractproperty	
	def cargarDinero(self,data):
		pass
