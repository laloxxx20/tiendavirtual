# from abc import ABCMeta
import abc

class GenericaDB():
	__metaclass__ = abc.ABCMeta

	@abc.abstractproperty	
	def obtenerUno(self,data): #obtiene uno por email
		pass

	@abc.abstractproperty		
	def obtenerTodos(self,data):
		pass 		

	@abc.abstractproperty	
	def guardar(self,data):
		pass	

	@abc.abstractproperty		
	def contar(self):
		pass

	@abc.abstractproperty	
	def existe(self,data):
		pass

	@abc.abstractproperty		
	def actualizar(self,data,identificador):
	 	pass

	# @abc.abstractproperty		
	# def crear_usuario(self,data):
	# 	return "desde clase padre"

	# @abc.abstractproperty		
	# def find_user(self,data):
	# 	return "desde padre find user"

