from Productos.models import ProductosBD, MediaBD
from BaseDatos.models import ProductoGene
from django.core.exceptions import ObjectDoesNotExist

class ProducBD(ProductoGene):

	def guardar(self,data):
		try:		
			m=MediaBD(tipo=data[0],autor=data[1],extension=data[2],mime_type=data[3],categoria=data[4])
			m.save()	
			p=ProductosBD(media_id=m.id,precio=data[5],nombre=data[6])
			p.save()			

		except  Exception, err: 
			print 'ERROR:', str(err)	

		return "desde clase hija"		

	def obtenerUno(self,data):
		pass
	
	def obtenerTodos(self,data):
		pass
	
	def contar(self):
		pass
	
	def existe(self,data): # retorna el objeto de la BD si existe si no  None
		pass
	
	def actualizar(self,data,identificardor):
	 	pass