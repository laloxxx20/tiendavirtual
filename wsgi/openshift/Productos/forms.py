# -*- coding: utf-8 -*-
from django import forms
from Nucleo.Usuario import *
from django.core.mail import send_mail
from django.core.exceptions import ValidationError
import string
import hashlib
from random import sample, choice

class RegisterForm(forms.Form):
	usuario = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'login username-field',
															 'id':'username',
															 'placeholder':'Usuario',
															 'required':''}))	
	nombre= forms.CharField(widget = forms.TextInput(attrs = {'class' : 'login username-field',
															 'id':'nombre',
															 'placeholder':'Nombre',
															 'required':''}))	
	apellidos = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'login username-field',
															 'id':'apellidos',
															 'placeholder':'Apellidos',
															 'required':''}))	
	correo = forms.EmailField(widget = forms.TextInput( attrs = {'class' : 'login email-field',
																'name':'email',
																'id':'email',
																'placeholder':'example@example.com',
																'required':''}))	
	clave = forms.CharField(widget = forms.PasswordInput( attrs = {'class' : 'login password-field',
																'name':'password',
																'id':'clave',
																'placeholder':'clave',
																'required':''}))	

	clave_confirmar = forms.CharField(widget = forms.PasswordInput( attrs = {'class' : 'login password-field',
																'name':'password',
																'id':'clave_confirmar',
																'placeholder':'Confirma clave',
																'required':''}))

	

	def clean(self):		
		cleaned_data = super(RegisterForm	, self).clean()		
		usuario=cleaned_data.get("usuario")
		correo=cleaned_data.get("correo")
		clave=cleaned_data.get("clave")
		clave_confirmar=cleaned_data.get("clave_confirmar")

		usuarioTotal = Usuario()		
		usuPorNombre = usuarioTotal.obtener_usuario(usuario)		
		usuPorCorreo = usuarioTotal.obtener_correo(correo)

		if usuPorNombre != None:
			raise forms.ValidationError("Este Usuario ya existe, intenta con otro!")	

		if usuPorCorreo != None:
			raise forms.ValidationError("Este Correo ya existe, intenta con otro!")

		if clave != clave_confirmar:
			raise forms.ValidationError("Tus claves no coinciden, intenta denuevo!")

		return cleaned_data																	

	def enviar_correo (self, correo):
		titulo, del_correo, a = u'Bienvenido a TiendaVirtual', 'Nueva Cuenta', correo		
		chars = string.letters + string.digits
		# clave = u''.join(choice(chars) for _ in range(8))
		# text_content = "Tu cuenta ha sido registrada"
		html_contenido =  correo + u' Gracias por registrarte en nuestro servicio.' 
		# send_mail(titulo, html_contenido, del_correo, [a])
		clave= self.cleaned_data.get("clave")
		return clave

	def registrar(self):	
		nombreP_db = self.cleaned_data['nombre']
		apellidos_db = self.cleaned_data['apellidos']		
		correoP_db = self.cleaned_data['correo']
		claveP_db = self.enviar_correo (correoP_db)
		usuP_db = self.cleaned_data['usuario']
		usuarioTotal=Usuario()
		usuarioTotal.Registrarse(nombreP_db,apellidos_db,"",claveP_db,usuP_db,correoP_db,0.00,0)
