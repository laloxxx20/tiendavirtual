from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

TIPOS = (
 (0, 'Video'),
 (1, 'Audio'),
 (2, 'Imagenes'),
)

EXTENSIONES=(
 (0, '.wmv'),
 (1, '.avi'),
 (2, '.mpg'),
 (3, '.mov'),
 (4, '.mp3'),
 (5, '.wav'),
 (6, '.mid'),
 (7, '.png'),
 (8, '.jpg'),
 (9, '.gif'),
 (10, '.bmp'),
)

MIME_TYPES=(
 (0, 'wmv'),
 (1, 'avi'),
 (2, 'mpg'),
 (3, 'mov'),
 (4, 'mp3'),
 (5, 'wav'),
 (6, 'mid'),
 (7, 'png'),
 (8, 'jpg'),
 (9, 'gif'),
 (10, 'bmp'),
)

CATEGORIAS=(
 (0, 'humor'),
 (1, 'terror'),
 (2, 'accion'),
 (3, 'naturaleza'),
 (4, 'musica'),
 (5, 'conferencias'),
 (6, 'peliculas'),
 (7, 'documentales'),
 (8, 'ciudades'),
 (9, 'rock'),
 (10, 'pop'),
)

class MediaBD(models.Model):
	tipo = models.IntegerField(choices=TIPOS)
	autor = models.CharField(max_length=50)
	extension = models.IntegerField(choices=EXTENSIONES)
	mime_type = models.IntegerField(choices=MIME_TYPES)
	categoria = models.IntegerField(choices=CATEGORIAS)

class ProductosBD(models.Model):
	media = models.ForeignKey(MediaBD)
	# archivo = models.FileField(upload_to='attachments')
	archivo = models.ImageField(upload_to='data/%Y/%m/%d/')
	precio= models.FloatField()		
	nombre= models.CharField(max_length=50)

	@property
	def filename(self):
		return self.archivo.name.rsplit('/', 1)[-1]

class PromocionBD(models.Model):		 
	fecha_inicio=models.IntegerField()
	fecha_fin=models.IntegerField()
	descuento = models.FloatField()
	descripcion = models.CharField(max_length=200)

# class PersonaBD(models.Model):
# 	nombre=models.CharField(max_length=50)	
# 	seg_apellido=models.CharField(max_length=50)
# 	pri_apellido=models.CharField(max_length=50)
# 	usuario=models.CharField(max_length=30)
# 	clave=models.CharField(max_length=100)
# 	correo=models.CharField(max_length=100)
# 	saldo = models.FloatField()
# 	estado_cuenta = models.BooleanField(default = True)
# 	super_user=models.BooleanField(default = False)

class Usuario_perfil(models.Model):
	user = models.OneToOneField(User)
	num_card = models.IntegerField(default=0)
	saldo = models.FloatField(default=0)
	estado_cuenta = models.BooleanField(default = True)

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Usuario_perfil.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)    	


class DescargasBD(models.Model):
	fecha=models.IntegerField()
	productos = models.ForeignKey(ProductosBD)
	promocion = models.ForeignKey(PromocionBD)
	persona = models.ForeignKey(User)
	nota = models.IntegerField(default=0)
	
class UploadModel(models.Model):
    title = models.CharField(max_length=64, blank=True)
    file = models.ImageField(upload_to='uploads/%Y/%m/%d/')

    @property
    def filename(self):
        return self.file.name.rsplit('/', 1)[-1]