from django.conf.urls import patterns, url
from Productos import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	# url(r'^probandoproyecto/$', views.probando, name='probando'),
	url(r'^registrarse/$', views.creando_user, name='crear_user'),
	url(r'^iniciar_sesion/$',views.iniciar_sesion, name='iniciar_sesion'),
)