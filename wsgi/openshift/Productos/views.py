from django.http import HttpResponseRedirect, HttpResponse, HttpRequest
from django.shortcuts import get_object_or_404, render, get_list_or_404, render_to_response# from Nucleo.Usuario import *
from Productos.forms import *
from django.core.urlresolvers import reverse


def index(request):					
	context={'title':'Inicio' }	
	return render(request,'inicio.html',context)

def creando_user(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			form.registrar()
			return HttpResponseRedirect ( reverse ('Productos:iniciar_sesion') )
	else:
		form = RegisterForm()		
	
	context={'title':'Registrarse','formRegister':form }	
	return render(request,'registrarse.html',context)

#def borrar_user(request):
#	if request.method == 'POST':



def iniciar_sesion(request):
	# usuario=Usuario()
	# usuario.iniciar_secion("pio", "garciaita")
	context={'title':'Inicio Sesion' }	
	return render(request,'inicio_sesion.html',context)


	
#def perfil(request):
#	return render(request,'perfil.html')

	
